package org.example;

import java.util.Scanner;

public class PGDC {
    public static void PGDC(){
        System.out.println("Calcul du plus grand diviseur commun de 2 nombres");
        Scanner sc = new Scanner(System.in);
        System.out.println("Donnez moi un premier nombre: ");
        int nbr1 = sc.nextInt();
        System.out.println("Donnez moi un deuxieme nombre: ");
        int nbr2 = sc.nextInt();
        int a = nbr1;
        int b = nbr2;
        while(a!=b){
            if (a>b){
                a = a - b;
            } else{
                b = b - a;
            }
        }
        System.out.println("Le plus grand diviseur commun de " +nbr1+ " et " +nbr2+ " est " +a);
    }
}
