package org.example;

public class Main {
    public static void main(String[] args) {
        Door door = new Door("blue");
        Appartement appartement = new Appartement(door);
        Person person1 = new Person("Thomas", appartement);
        person1.display();
        appartement.display();
        door.display();

    }
}