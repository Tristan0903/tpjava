package org.example;

public class Person {
    public String prenom;
    public Appartement house;

    public void display(){
        System.out.println("Je m'appelle " +prenom);
        System.out.println(house);
        System.out.println(house.getDoor());

    }

    public String getPrenom() {
        return prenom;
    }

    public Person(String prenom, Appartement house) {
        this.prenom = prenom;
        this.house = house;
    }

}
