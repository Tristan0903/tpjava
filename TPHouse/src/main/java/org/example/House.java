package org.example;

import javax.swing.plaf.IconUIResource;

public class House {
    public int surface;

    public Door door;

    public Door getDoor() {
        return door;
    }

    public void display() {
      toString();
    }

    public int getSurface() {
        return surface;
    }

    public void setSurface(int surface) {
        this.surface = surface;
    }


    public House(int surface, Door door) {
        this.surface = surface;
        this.door = door;
    }

    @Override
    public String toString() {
        return "House{" +
                "surface=" + surface +
                ", door=" + door +
                '}';
    }
}
