package org.example;

import org.example.enums.DemoEnum;
import org.example.method.MethodParam;
import org.example.operator.Operator;
import org.example.poo.Phone;
import org.example.poo.Product;
import org.example.poo.inner.Outer;
import org.example.readFromConsole.ReadFromConsole;
import org.example.string.ChaineCaractere;
import org.example.string.ChaineCaractere1;
import org.example.structure.Structure;
import org.example.structure.Structure2;
import org.example.variable.Variable;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
//        Product p = new Product();
//        Product p2 = new Product(1, "Tristan");
//        p.setName("Julien");
//        System.out.println(p.getName());
//        System.out.println(p2.getName());
//
//        Phone phone = new Phone(12);
//        phone.setPrix(13);
//        phone.setName("Iphone");
//        phone.setId(3);
//        System.out.println(phone.toString());
//        Phone phone1 = new Phone(1, "Nokia", 45);
//        System.out.println(phone1.toString());
//
//        phone.whatIAme();
//        p.whatIAm();

        Outer outerCLass = new Outer();

        Outer.InnerClass inner = outerCLass.new InnerClass();

        inner.afficheMessage2();

        Outer.OtherClassInner otherClassInner = new Outer.OtherClassInner();
        otherClassInner.afficherMessage();



//        Variable.getVariable();
//        ReadFromConsole.getReadWriteConsole();
//        ChaineCaractere.getMethodString();
//        ChaineCaractere.getComparaisonString();
//        ChaineCaractere.getFormatting();
//        ChaineCaractere1.getRegex();
//        Structure.getIfElse();
//        Structure.getIfElseWithoutBraces();
//        Structure.getIfElseIfElse();
//        Structure.getSwitch();
//        Structure2.getBoucleFor();
//        Operator.getOperators();
//        Operator.getExpressionType();
//        Operator.getOperationAndComparaisonType();
//        int i = 10;
//        MethodParam.changeIntValue(i);
//        System.out.println("valeur i " +i);
//        i = MethodParam.changeInt(i);
//        System.out.println("valeur i" +i);
//         int[] array = {1,2,3};
//        System.out.println("Array avant methode changeArray" + Arrays.toString(array));
//        MethodParam.changeArray(array);
//
//        System.out.println("Array après méthode changeArray" +Arrays.toString(array));
//
//        System.out.println("class : " +array.getClass());
//        DemoEnum.getEnum();

    }
}