package org.example.poo.package2;

import org.example.poo.package1.ClassC;

public class Demo {


    public static void main(){
        ClassB classB = new ClassB();
        ClassC classC = new ClassC();

        classC.doSomethingB();
        classC.doSomethingB2();

        classB.doSomethingB();
        classB.doSomethingB2();
        classB.doSomethingB3();
    }
}
