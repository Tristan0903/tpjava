package org.example.string;

import java.util.Arrays;

public class ChaineCaractere {

    static String firstName = "";

    public static void getMethodString() {
        System.out.println("\n ----- Les chaînes de caractères -----\n");
        String s = "Hello";
        System.out.println("s.length() " + s.length());
        System.out.println("s.contains(\"he\") " + s.contains("he"));
        System.out.println("s.isEmpty() " + s.isEmpty());
        System.out.println("s.toUpperCase() " + s.toUpperCase());
        System.out.println("s.startWith(\"h\") " + s.startsWith("H"));
        System.out.println("s.endsWith(\"o\") " + s.endsWith("o"));
        System.out.println("s.replace(\"ll\",\"LL\") " + s.replace("ll", "LL"));
        System.out.println("s.trim() " + s.trim());
        System.out.println("s.substring(0,3) " + s.substring(0, 3));
        System.out.println("Arrays.toString(s.getBytes()) " + Arrays.toString(s.getBytes()));
        System.out.println("Arrays.toString(s.toCharArray()) " + Arrays.toString(s.toCharArray()));
        System.out.println("s.charAt(1) " + s.charAt(1));
        System.out.println("Arrays.toString(s.split(\"e\")) " + Arrays.toString(s.split("e")));
        System.out.println(s);
    }

    public static void getComparaisonString() {
        System.out.println("\n ----- Comparaison String -----\n");
        String s = " hello ";
        String s2 = " hello ";
        System.out.println("s == s2 " + (s == s2));
        String s3 = new String(" hello ");
        System.out.println("s == s3 " + (s == s3));
        System.out.println("s.equals(s3) " + s.equals(s3));
        firstName = "Lucas";
        String firstName1 = "lucas";
        System.out.println("firstName.equals(firstName1) " + firstName.equals(firstName1));
        System.out.println("firstName.equalsIgnoreCase(firstName1) " + firstName.equalsIgnoreCase(firstName1));
    }

    public static void getFormatting() {
        System.out.println("\n ----- Formattage String -----\n");
        firstName = "Abdallah";
        String phrase = "Salut, cher %s! Good %s.";
        String morning = "morning";
        String afternoon = "afternoon";
        String evening = "evening";
        String nouvellePhrase = String.format(phrase, firstName, morning);
        System.out.println(nouvellePhrase);
        System.out.printf("Vous allez gagner %d à l'euromillion dans %d jours.\n", 100000, 10);
    }
}
