package org.example.readFromConsole;

import java.util.Scanner;

public class ReadFromConsole {
    public static void getReadWriteConsole() {
            Scanner sc = new Scanner(System.in);
            System.out.println("Donne moi un mot : ");
            String mot = sc.next();
        System.out.println("Le mot donné est : " + mot);
        System.out.println("\nDonne moi un nombre : ");
        int nombre = sc.nextInt();
        System.out.println("Le nombre donné est : " + nombre);
    }
}
