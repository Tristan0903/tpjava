package org.example.structure;

public class Structure2 {
    public static void getBoucleFor() {
        int[] tab = {1, 2, 3};
        int sum = 0;
        System.out.println("");
        for (int i = 0; i <= 5; i++) {
            System.out.println("i = " + i);
        }
        System.out.println("");
        for (int i = 0, j = 10; i < j; i++, j--) {
            System.out.println("Counters i = " + i + " et j = " + j);
        }
        System.out.println("");
        for (int number : tab) {
            sum += number;
            System.out.println("Number = " + number);
        }
        System.out.println("La somme est de " + sum);

    }

    public static void getBreakAndContinue() {
        System.out.println("--- continue ---");
        for (int i = 0; i <= 5; i++) {
            if (i % 2 == 0) {
                System.out.println("je suis dans le if " + i);
                continue;
            }
            System.out.println("counter :" + i);
        }
        System.out.println("--- break ---");
        for (int i = 0; i <= 5; i++) {
            if (i == 3) {
                break;
            }
            System.out.println("counter " + i);
        }
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 5; j++) {
                if (i == 3) {
                    break;
                }
                System.out.println(j + " ");
            }
            System.out.println("counter" + i);
        }
    }
}
