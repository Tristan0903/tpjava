package org.example.structure;

public class Structure {

    static int montant = 100;
    static int montant1 = 200;

    public static void getIfElse() {
        System.out.println("");
        if (montant > montant1) {
            System.out.println("Le montant est supérieur au montant1.");
        } else {
            System.out.println("Le montant est inférieur au montant1.");
        }
    }

    public static void getIfElseWithoutBraces() {
        System.out.println("");
        if (montant > montant1)
            System.out.println("Le montant est supérieur au montant1.");
        else
            System.out.println("Le montant est inférieur au montant1.");
    }

    public static void getIfElseIfElse() {
        System.out.println("");
        if (montant > montant1)
            System.out.println("Le montant est supérieur au montant1.");
        else if (montant < montant1)
            System.out.println("Le montant est inférieur au montant1.");
        else if (montant == montant1)
            System.out.println("Le montant est égale au montant1.");
        else
            System.out.println("Impossible");
    }

    public static void getSwitch() {
        String val = "toto";
        System.out.println("");
        switch (val) {
            case "titi":
                System.out.println("C'est titi");
                break;
            case "tata":
                System.out.println("C'est tata");
                break;
            case "tutu":
                System.out.println("C'est tutu");
                break;
            case "tyty":
                System.out.println("C'est tyty");
                break;
            case "toto":
                System.out.println("C'est toto");
                break;
            default:
                System.out.println("Aucun cas ne correspond.");
        }
    }
}
