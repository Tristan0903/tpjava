package org.example.Enums;

import java.sql.SQLOutput;
import java.util.Scanner;

public class EnumHelper {
    public static void getEnumWithMess(){
        System.out.println("Veuillez entrer une valeur comprise entre A et D");
        Scanner sc = new Scanner(System.in);
        MessageType message = MessageType.valueOf(sc.next().toUpperCase());
        while (message != MessageType.A && message!=MessageType.B && message!=MessageType.C && message!=MessageType.D){
            System.out.println("Veuillez saisir une autre valeur");
        }
        switch(message){
            case A:
                System.out.println(Priority.valueOf("HIGH"));
                break;
            case B:
                System.out.println(Priority.valueOf("MEDIUM"));
                break;
            case C:
                System.out.println(Priority.valueOf("LOW"));
                break;
            case D:
                System.out.println(Priority.valueOf("LOW"));
        }
    }
}
