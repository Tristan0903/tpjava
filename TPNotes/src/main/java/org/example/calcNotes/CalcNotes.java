package org.example.calcNotes;

import java.util.ArrayList;
import java.util.Scanner;

public class CalcNotes {

    public static void calcNotes(){
        Scanner sc = new Scanner(System.in);
        int total = 0;
        int valMax = 0;
        int valMin=21;
        ArrayList<Integer> notes = new ArrayList<>();
        System.out.println("Entrez les notes des élèves: ");
        for (int i=0; i<20; i++){
            int entrée = sc.nextInt();
            notes.add(entrée);
            total = total+entrée;
            if (i==0){
                valMin = entrée;
                valMax = entrée;
            } else if(entrée<valMin){
                valMin = entrée;
            } else if (entrée>valMax){
                valMax = entrée;
            }
        } int moyenne = total/20;
        System.out.println("La plus grande note est "+valMax);
        System.out.println("La plus petite note est "+valMin);
        System.out.println("La moyenne de la classe est de " +moyenne);
    }}

