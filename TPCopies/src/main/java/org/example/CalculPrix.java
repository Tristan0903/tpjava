package org.example;

public class CalculPrix {
    public static void calcCopies(int copies){
        if(copies<10){
            int prixTotal = 15*copies;
            System.out.println(prixTotal+ " centimes.");
        } else if (10<=copies && copies<=20){
            int prixTotal = 10*copies;
            System.out.println(prixTotal/100+ " euros.");
        } else if(copies>20){
            int prixTotal = 5*copies;
            System.out.println(prixTotal/100+ " euros.");
        }

    }
}
