package org.example;

public class OccurenceCount {
    public static void countOccurence(String arg){
        int occurence = 0;
        char lettre = 'l';
        for(int i=0; i<arg.length(); i++){
            if (arg.charAt(i) == lettre){
                occurence++;
            }
        }
        System.out.println("L'occurence de la lettre " +lettre+ " du mot "  +arg+ " est " +occurence);
    }
}
