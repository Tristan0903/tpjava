package org.example;

public class PalindrommeVerif {
    public static void verifPalindrome(){
        int compteur =0;
        String word = "kayak";
        char[] wordArray = word.toCharArray();
        for (int i=0; i<word.length(); i++){
            for (int j=word.length()-1; j>0; j--){
                if (wordArray[i] == wordArray[j]){
                    compteur++;
                    break;
                }
            }

        }     if(compteur == word.length()){
            System.out.println("Le mot " +word+ " est un palindrome");
        } else {
            System.out.println("Le mot " +word+ " n'est pas un palindrome");
        }
    }
}
