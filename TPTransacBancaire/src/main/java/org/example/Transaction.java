package org.example;

public class Transaction {

  static int count =0;

   int id;
    private Account compteDe;
    private Account compteA;
    private double moneyAmount;
    private StandardAccountOperations operation;


    public Transaction() {
    }

    public Transaction(Account compteDe, Account compteA, double moneyAmount, StandardAccountOperations operation) {
        this.id = count++;
        this.compteDe = compteDe;
        this.compteA = compteA;
        this.moneyAmount = moneyAmount;
        this.operation = operation;
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return "Transaction{" +
                ", id=" + id +
                ", moneyAmount=" + moneyAmount +
                ", operation=" + operation +
                '}';
    }
}
