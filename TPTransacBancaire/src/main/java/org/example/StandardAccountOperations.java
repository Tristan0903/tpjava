package org.example;

public enum StandardAccountOperations {
    MONEY_TRANSFER_SEND("Votre envoi d'argent a bien été effectué."),
    MONEY_TRANSFER_RECEIVE("Vous avez bien reçu l'argent."),
    CANCEL_OPERATION("Vous avez bien annulé l'opération.");


    private String infoTransac;
    private StandardAccountOperations(String infoTransac){
        this.infoTransac = infoTransac;
    }
}
