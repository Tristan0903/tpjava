package org.example;

import java.util.ArrayList;
import java.util.List;

public class Account implements AccountInterface {
    static int count = 0;
    int id;
    private List<Transaction> transacs = new ArrayList<Transaction>();
    private double solde;

    public Account() {

    }

    public Account(List<Transaction> transacs, int solde) {
        this.id = ++count;
        this.transacs = transacs;
        this.solde = solde;
    }

    public int getId() {
        return id;
    }

    @Override
    public void sendMoneyToAccount(Account accountTo, double moneyAmount, Account accountFrom) {
        accountTo.solde +=moneyAmount;
        receiveMoney(accountFrom, moneyAmount);
    }

    @Override
    public void receiveMoney(Account accountFrom, double moneyAmount) {
        accountFrom.solde -= moneyAmount;
    }

    @Override
    public void AfficheTransactions() {
        for (Transaction transactions:
             transacs) {
            System.out.println(transactions);
        }
    }

    public List<Transaction> getTransactions() {
        return this.transacs;
    }

    public Transaction getTransactionsByid(int id) {
        Transaction transaction = new Transaction();
        for (Transaction transactions:transacs) {
            if(transactions.getId() == id){
                transaction = transactions;
               return transaction;
            }else{
                return null;
            }
        }
        return transaction;
    }

    public double getSolde() {
        return solde;
    }

    @Override
    public String toString() {
        return "Account{" +
                "id=" + id +
                ", transacs=" + transacs +
                ", solde=" + solde +
                '}';
    }
}
