package org.example;

import java.util.List;

public interface AccountInterface {
    public void sendMoneyToAccount(Account accountTo, double moneyAmount, Account accountFrom);

    public void receiveMoney(Account accountFrom, double moneyAmount);

    public void AfficheTransactions();

    public List<Transaction> getTransactions();
}
