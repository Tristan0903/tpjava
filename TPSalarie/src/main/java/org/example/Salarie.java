package org.example;

public class Salarie {
    public int compteur = 0;

    public int count;
    public String matricule;
    public String categorie;
    public String service;
    public String nom;
    public int salaire;



    public void AfficheNombreSalarie(int compteur){
        System.out.println("Le nombre de salariés est de: " +compteur);
    }

    public void AfficherSalaire(String nom, int salaire){
        System.out.println("Le salaire de " +nom+ " est de " +salaire);
    }

    public Salarie(String matricule, String categorie, String service, String nom, int salaire) {
        this.matricule = matricule;
        this.categorie = categorie;
        this.service = service;
        this.nom = nom;
        this.salaire = salaire;
        this.count = compteur++;
    }
}
