package org.example;

public class Main {
    public static void main(String[] args) {
        Salarie newSalarie = new Salarie("Jean", 2100);
        Salarie newSalarie2 = new Salarie("Maurice", 1750);
        Salarie newSalarie3 = new Salarie("Laurent", 3150);
        Salarie newSalarie4 = new Salarie("Christophe", 1300);

        System.out.println("Le nombre de salariés : " + Salarie.getCompteurInstance());

        float sommeSalaire = Salarie.somme(newSalarie.salaire, newSalarie2.salaire, newSalarie3.salaire, newSalarie4.salaire);

        newSalarie.afficherSalaire();
        newSalarie2.afficherSalaire();
        newSalarie3.afficherSalaire();
        newSalarie4.afficherSalaire();
        System.out.println("Le montant total des salaires mensuels est de " + sommeSalaire);

        Salarie.setCompteurInstance(1);

        System.out.println("Le nombre de salariés : " + Salarie.getCompteurInstance());

    }
}