package org.example;

public class Publisher {

    static int count;
    private int id;
    private String publisherName;

    public Publisher() {
    }

    public Publisher(String publisherName) {
        this.id = count++;
        this.publisherName = publisherName;
    }

    @Override
    public String toString() {
        return "Publisher: " +publisherName;
    }

    public int getId() {
        return id;
    }

    public String getPublisherName() {
        return publisherName;
    }

    public void setPublisherName(String publisherName) {
        this.publisherName = publisherName;
    }
}
