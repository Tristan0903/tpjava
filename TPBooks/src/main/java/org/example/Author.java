package org.example;

public class Author {

    static int count;
    private int id;
    private String firstName;
    private String lastName;



    public Author(){

    }


    public Author(String firstName, String lastName) {
        this.id = count++;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    @Override
    public String toString() {
        return "Author's Firstname: "+firstName+
                "Author's Lastname" +lastName;
    }

    public int getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
