package org.example;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

public class Book {
    private int id;
    private String name;
    private  Author[] authors;
    private Publisher publisher;
    private int PublishingAnnee;
    private int amountOfPages;
    private BigDecimal price;
    private CoverType coverType;


    public Book() {
    }

    public Book(int id, String name, Author[] authors, Publisher publisher, int publishingAnnee, int amountOfPages, BigDecimal price, CoverType coverType) {
        this.id = id;
        this.name = name;
        this.authors = authors;
        this.publisher = publisher;
        this.PublishingAnnee = publishingAnnee;
        this.amountOfPages = amountOfPages;
        this.price = price;
        this.coverType = coverType;
    }

    @Override
    public String toString() {
        return "Book: " +name+
                "Book authors: " +authors+
                "Book publisher: " +publisher+
                "Publishing Year: " +PublishingAnnee+
                "Price: "+price+
                "Cover Type: "+coverType;
    }

    public static void filterBooksByAuthor(Author author, List<Book> books){
        for (Book book:
             books) {
            for(Author authors: book.getAuthors()){
                if(authors.getFirstName().equals(author.getFirstName())){
                    System.out.println(book.getName()+ " a bien été écrit par "+authors.getFirstName());
                }
            }
        }
    }

    public static void filterBooksByPublisher(Publisher publisher, List<Book> books){
        for (Book book:
                books) {
                if(book.getPublisher().getPublisherName() == publisher.getPublisherName()){
                    System.out.println(book.getName() + " a bien été édité par "+publisher.getPublisherName());
                }
            }

        }

    public static void filterBooksAfterSpecifiedYear(int yearFromInclusievly, List<Book> books){
        for (Book book:
             books) {
            if(book.getPublishingAnnee()>=yearFromInclusievly){
                System.out.println("Le livre " +book.getName()+ " a bien été publié après " +yearFromInclusievly);
            }
        }
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Author[] getAuthors() {
        return authors;
    }

    public Publisher getPublisher() {
        return publisher;
    }

    public int getPublishingAnnee() {
        return PublishingAnnee;
    }

    public int getAmountOfPages() {
        return amountOfPages;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public CoverType getCoverType() {
        return coverType;
    }
}
