package org.example;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class Demo7 {
    public static void demo() {
        Book book1 = new Book(1, "Book_1", new Author[]{new Author("Jon", "Johnson")}, new Publisher("Publisher_1"), 1990, 231, BigDecimal.valueOf(24.99), CoverType.PAPERBACK);
        Book book2 = new Book(2, "Book_2", new Author[]{new Author("Jon", "Johnson"), new Author("William", "Wilson")}, new Publisher("Publisher_2 "), 2000, 120, BigDecimal.valueOf(14.99), CoverType.PAPERBACK);
        Book book3 = new Book(3, "Book_3", new Author[]{new Author("Walter", "Peterson")}, new Publisher("Publisher_1"), 1997, 350, BigDecimal.valueOf(34.99), CoverType.HARDCOUVERTURE);
        Book book4 = new Book(4, "Book_4", new Author[]{new Author("Craig", "Gregory")}, new Publisher("Publisher_3"), 1992, 185, BigDecimal.valueOf(19.99), CoverType.PAPERBACK);
        List<Book> books = new ArrayList<Book>();
        books.add(book1);
        books.add(book2);
        books.add(book3);
        books.add(book4);
        Author author1 = new Author("Craig", "Gregory");
        Publisher publisher1 = new Publisher("Publisher_1");
        int year = 1998;
        Book.filterBooksByAuthor(author1, books);
        Book.filterBooksByPublisher(publisher1, books);
        Book.filterBooksAfterSpecifiedYear(year, books);
    }
}
