package org.example.DrawRectangle;

import java.util.Scanner;

public class DrawRectangle {
    public static void drawRectangle(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Donnez moi la hauteur du rectangle: ");
        int hauteur = sc.nextInt();
        System.out.println("Donnez moi la largeur du rectangle: ");
        int largeur = sc.nextInt();
        int hauteurMax = hauteur;
        do {
            if(hauteur == 1 || hauteur == hauteurMax){
                String result = new String(new char[largeur]).replace("\0", "*");
                System.out.println(result);
                hauteur--;
            } else{
                String result = new String(new char[largeur-2]).replace("\0", " ");
                System.out.println("*"+result+"*");
            hauteur--;}
            } while (hauteur!=0);

    }
}
