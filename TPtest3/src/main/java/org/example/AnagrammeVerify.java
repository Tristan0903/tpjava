package org.example;

import java.util.Arrays;
import java.lang.reflect.Array;

public class AnagrammeVerify {
    public static void verifAnagram (){
        int compteur = 0;
        String mot1 = "romain";
        String mot2 = "manoir";
        char[] mot1bis = mot1.toCharArray();
        char[] mot2bis = mot2.toCharArray();
        Arrays.sort(mot1bis);
        Arrays.sort(mot2bis);
        for (int i=0; i<mot1.length(); i++){
            for (int j=0; j<mot2.length(); j++){
                if(mot1bis[i] == mot2bis[j]){
                    System.out.println(mot1bis[i]+ " est égal à " +mot2bis[j]);
                    compteur++;
                    break;
                }
            } if (compteur == mot1.length()){
                System.out.println("Les deux mots sont des anagrammes");
            }
        }
    }
}
