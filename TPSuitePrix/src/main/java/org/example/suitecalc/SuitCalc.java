package org.example.suitecalc;

import java.util.Scanner;

public class SuitCalc {
    public static void calcSuit(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Donnez moi un prix : ");
        int entrée = sc.nextInt();
        int total = entrée;
        while(entrée != 0){
            entrée = sc.nextInt();
            total = total + entrée;
        }
        System.out.println("Le total pour vos articles est de " +total);
    }
}
