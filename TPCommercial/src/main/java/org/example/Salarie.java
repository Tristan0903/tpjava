package org.example;

public class Salarie {

    public String matricule;
    public String categorie;
    public String service;
    public String name;
    public float salaire;
    public static int compteurInstance = 0;

    public Salarie(String name, float salaire) {
        this.name = name;
        this.salaire = salaire;
        ++compteurInstance;
    }

    public Salarie(){

    }

    public float calculerSalaire(){
        return salaire;
    }




    public void afficherSalaire() {
        System.out.println("Le salaire de " + name + " est de " + salaire + "€.");
    }

    public static int somme(float... floats) {
        int res = 0;
        for (float i : floats) {
            res += i;
        }
        return res;
    }
}
