package org.example;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        List<Salarie> salariesList = new ArrayList<Salarie>();
        List<Commercial> commercialList = new ArrayList<Commercial>();
        Scanner sc = new Scanner(System.in);
        Salarie newSalarie = new Salarie("Jean", 2100);
        salariesList.add(newSalarie);
        Salarie newSalarie2 = new Salarie("Maurice", 1750);
        salariesList.add(newSalarie2);
        Salarie newSalarie3 = new Salarie("Laurent", 3150);
        salariesList.add(newSalarie3);
        Salarie newSalarie4 = new Salarie("Christophe", 1300);
        salariesList.add(newSalarie4);
        Salarie newSalarie5 = new Salarie("Tristan", 1500);
        salariesList.add(newSalarie5);
        Salarie newSalarie6 = new Salarie("Sylvain", 2600);
        salariesList.add(newSalarie6);
        int prop;
        do {
            System.out.println("==== Gestion des employés ====");
            System.out.println("1-- Ajouter un employé");
            System.out.println("2-- Afficher le salaire des employés");
            System.out.println("3-- Rechercher un employé");
            System.out.println("0-- Quitter");
            System.out.println("Entrez votre choix: ");
            prop = sc.nextInt();
            switch (prop) {
                case 1:
                    System.out.println("==== Ajouter un employé ====");
                    System.out.println("1-Salarié");
                    System.out.println("2-Commercial");
                    System.out.println("0-Retour");
                    System.out.println("Entrez votre choix: ");
                    int prop2 = sc.nextInt();
                    switch (prop2) {
                        case 1:
                            System.out.println("Merci de saisir le nom: ");
                            String name = sc.next();
                            System.out.println("Merci de saisir le matricule:");
                            String matricule = sc.next();
                            System.out.println("Merci de saisir la catégorie: ");
                            String categorie = sc.next();
                            System.out.println("Merci de saisir le service: ");
                            String service = sc.next();
                            System.out.println("Merci de saisir le salaire: ");
                            int salaire = sc.nextInt();
                            Salarie salarie = new Salarie(name, salaire);
                            salariesList.add(salarie);
                            break;
                        case 2:
                            System.out.println("Merci de saisir le nom: ");
                            String nameCom = sc.next();
                            System.out.println("Merci de saisir le matricule:");
                            String matriculeCom = sc.next();
                            System.out.println("Merci de saisir la catégorie: ");
                            String categorieCom = sc.next();
                            System.out.println("Merci de saisir le service: ");
                            String serviceCom = sc.next();
                            System.out.println("Merci de saisir le salaire: ");
                            int salaireCom = sc.nextInt();
                            System.out.println("Merci de saisir le chiffre d'affaire: ");
                            int ca = sc.nextInt();
                            System.out.println("Merci de saisir la commission: ");
                            int commission = sc.nextInt();
                            Commercial salarieCom = new Commercial(nameCom, salaireCom, commission);
                            commercialList.add(salarieCom);
                            break;
                            default: break;

                    }
                    break;
                case 2:
                    System.out.println("==== Salaire des employés ====");
                    System.out.println("--------");
                    for (Salarie salaries : salariesList) {
                        salaries.afficherSalaire();
                    }
                    System.out.println("--------");
                    for (Commercial commercials : commercialList) {
                        commercials.afficherSalaire();
                    }
                    break;
                case 3 :
                    System.out.println("Veuillez saisir le nom de l'employe recherche: ");
                    sc.nextLine();
                    String name = sc.nextLine();
                    for (Salarie salaries: salariesList) {
                        if(salaries.name.equals(name)){
                            salaries.afficherSalaire();
                        }
                    }
                    for (Commercial commercials: commercialList
                         ) {
                        if(commercials.name.equals(name)){
                            commercials.afficherSalaire();
                        }
                    }
                    break;
            }

        } while (prop != 0);
    }
}