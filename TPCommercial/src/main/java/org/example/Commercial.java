package org.example;

public class Commercial extends Salarie{
    public float ca;
    public float commission;
    public int distance = 0;

    private final float POURCENTAGE = 100;

    public Commercial(){

    }

    public Commercial(String name, int salaire, int commission){
        super(name, salaire);
        this.commission = commission;
        this.ca = ca;
    }

    @Override
    public float calculerSalaire() {
        return super.calculerSalaire()+commission;
    }


    public void afficherSalaire() {
        System.out.println("Le salaire de " +name+ " est de " +salaire);
        float salairecommission = super.calculerSalaire()+(super.calculerSalaire()*(getCommission()/POURCENTAGE));
        System.out.println("Le salaire avec commission de " +name+ " est de " +salairecommission);
    }
    public void seDeplacer(){
        distance += 1000;
        System.out.println("Vous avez parcouru" +distance);
    }

    public float getCa() {
        return ca;
    }

    public float getCommission() {
        return commission;
    }

    public void setCa(int ca) {
        this.ca = ca;
    }

    public void setCommission(int commission) {
        this.commission = commission;
    }


}
